package de.zerofive.json;

import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TextField;
import lombok.Data;
import lombok.Getter;

import java.util.List;

@Getter
public class ServerInformation {
    public ServerInformation() {
       
        
    }

    private SimpleStringProperty loginUrl = new SimpleStringProperty();
    private SimpleStringProperty userProp = new SimpleStringProperty();
    private SimpleStringProperty pwProp = new SimpleStringProperty();
    private SimpleStringProperty tokenProp = new SimpleStringProperty();
    private SimpleStringProperty tokenRefreshUrl = new SimpleStringProperty();
    private SimpleStringProperty refreshTokenProp = new SimpleStringProperty();

    private SimpleStringProperty tokenHeaderProp = new SimpleStringProperty();
    private SimpleStringProperty tokenHeaderPrefix = new SimpleStringProperty();
    private SimpleMapProperty<String, String> additionalHeaders = new SimpleMapProperty<String, String>(FXCollections.observableHashMap());


    // Runtime
    private SimpleStringProperty currentToken  = new SimpleStringProperty();
    private SimpleStringProperty currentRefreshToken = new SimpleStringProperty();
    private SimpleStringProperty userName = new SimpleStringProperty();
    private SimpleStringProperty pw = new SimpleStringProperty();


}
