package de.zerofive.json;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.sun.source.tree.Tree;
import de.zerofive.core.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Pair;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthSchemeProvider;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.config.Lookup;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.auth.BasicSchemeFactory;
import org.apache.http.impl.auth.NTLMSchemeFactory;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.ProxyAuthenticationStrategy;
import org.apache.http.ssl.SSLContextBuilder;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.Timer;

@Slf4j
/**
 * Main Class and Window for this app.
 */
public class App extends Application {

    private TreeView treeView = null;
    private TreeView treeViewPost = null;
    private SimpleStringProperty getUrl = new SimpleStringProperty();
    private SimpleStringProperty postUrl = new SimpleStringProperty();
    private SimpleStringProperty baseUrl = new SimpleStringProperty();
    private String loginURL = null;
    private String refreshTokenURL = null;

    private TextField textFieldGet = null;
    private TextField textFieldPost = null;
    private JSONTreeItemGenerator itemGenerator = null;
    private LoginDialog loginDialog = null;
    private Button loginButton = null;
    private static Background BGDEFAULT = new Background(new BackgroundFill(Paint.valueOf("#EB3324"), new CornerRadii(5), Insets.EMPTY));
    private static Background BGSUCESS = new Background(new BackgroundFill(Paint.valueOf("#22B14C"), new CornerRadii(5), Insets.EMPTY));
    private StatusLabel statusLabelGet;
    private StatusLabel statusLabelPost;
    private Timer loginTimer;
    private TimerTask timerTask;
    private ObjectMapper mapper = new ObjectMapper();

    /**
     * Initializes the app and windows
     * Configuration, LoginDialog, Timer
     * @param stage the main stage
     * @throws Exception Any error
     */
    @Override
    public void start(Stage stage) throws Exception {
        // Exit App
        Platform.setImplicitExit(true);
        // No SSL verification at all

        Unirest.setHttpClient(noSSLVerificationClient());
        // Proxy settings
        String proxyHost = (String)((Z5Configuration)Z5Configuration.Instance()).get("zerofive.json.proxyHost").orElseGet(() -> null);
        Integer proxyPort = (Integer)((Z5Configuration)Z5Configuration.Instance()).get("zerofive.json.proxyPort").orElseGet(() -> null);
       /* HttpClientBuilder clientBuilder = HttpClientBuilder.create();


        CredentialsProvider credsProvider = new BasicCredentialsProvider();

        credsProvider.setCredentials(AuthScope.ANY, new NTCredentials(USERNAME, PASSWORD));

        clientBuilder.useSystemProperties();

        clientBuilder.setProxy(new HttpHost(proxyHost, Integer.valueOf(proxyPort)));
        clientBuilder.setDefaultCredentialsProvider(credsProvider);
        clientBuilder.setProxyAuthenticationStrategy(new ProxyAuthenticationStrategy());


        Lookup<AuthSchemeProvider> authProviders = RegistryBuilder.<AuthSchemeProvider>create()
                .register(AuthSchemes.NTLM, new NTLMSchemeFactory())
                .build();
        clientBuilder.setDefaultAuthSchemeRegistry(authProviders);


        Unirest.setHttpClient(clientBuilder.build());*/
        if(proxyHost != null && proxyPort != null)
        Unirest.setProxy(new HttpHost(proxyHost, proxyPort));
        // URL props
        getUrl.set((String) ((Z5Configuration)Z5Configuration.Instance()).get("zerofive.json.getURL").orElseGet(() -> "http://localhost:8080/sean"));
        postUrl.set((String) ((Z5Configuration)Z5Configuration.Instance()).get("zerofive.json.postURL").orElseGet(() -> "http://localhost:8080/sean"));
        baseUrl.set((String) ((Z5Configuration)Z5Configuration.Instance()).get("zerofive.json.baseURL").orElse(""));
        String color = (String) ((Z5Configuration)Z5Configuration.Instance()).get("zerofive.json.color").orElse("#2E693B");
        color =color.concat(";");
        // Draw the app
        BorderPane border = new BorderPane();
        HBox hbox = addHBox(color);
        border.setTop(hbox);

        Label baseLabel = new Label();
        baseLabel.setFont(Font.font("Verdana", FontWeight.BOLD, 13));
        baseLabel.setTextFill(Color.WHITESMOKE);
        if(baseUrl.getValue()!= null && !baseUrl.getValue().isEmpty()) {
            VBox box2 = new VBox();
            box2.setPadding(new Insets(10, 10, 15, 12));
            box2.setSpacing(30);
            box2.setStyle("-fx-background-color: " + color);
            baseLabel.setText("Base-URL " + baseUrl.getValueSafe());
            baseLabel.setAlignment(Pos.BOTTOM_LEFT);
            box2.getChildren().addAll(baseLabel);

            border.setBottom(box2);
        }

        // init item generator
        itemGenerator = JSONTreeItemGenerator.Instance();

        // draw trees
        border.setCenter(addGridPane());

        // init main window
        Scene scene = new Scene(border);
        stage.setScene(scene);
        stage.show();
        stage.setTitle("jsonator");

        // create login dialog
        loginDialog = new LoginDialog(stage, "login");

        // cell factories for drag and drop tree
        treeView.setCellFactory(new JSONDragCellFactory());
        treeViewPost.setCellFactory(new JSONDropCellFactory(stage));

        treeViewPost.setOnKeyReleased(event -> handleKeys(event, treeViewPost));
        //loginDialog.show();
        // Timer initialization
        loginTimer = new Timer("loginTimer");
        timerTask = new TimerTask() {
            @Override
            public void run() {
                if(loginDialog.doLogin()) {
                    loginButton.setBackground(BGSUCESS);
                } else
                {
                    loginButton.setBackground(BGDEFAULT);
                }
            }
        };
        loginTimer.schedule(timerTask, (Integer)(Z5Configuration.Instance().get("zerofive.json.logintimer").orElse(Integer.valueOf(5000))));

    }

    private void handleKeys(KeyEvent event, TreeView treeView) {
        // No Item or item
        TreeItem item =  treeView.getSelectionModel().getSelectedItem() != null ? (TreeItem)  treeView.getSelectionModel().getSelectedItem() : null;

        // No item no parent
        TreeItem parent = item != null ? item.getParent() : null;

        // new Item if not null we will add it
        TreeItem newItem = null;
        // Different new items on different keys
        switch (event.getCode()) {
            case M:
                newItem = new MapTreeItem("map");
                break;
            case E:
                newItem = new MapEntryTreeItem(new Pair("name", "value"));
                break;
            case L:
                newItem = new ListTreeItem("list");
                break;
            case P:
                newItem = new PrimitiveTreeItem("primitive");
                break;
            case DELETE:
                // we remove the item no matter where it is
                    if (parent != null) {
                        parent.getChildren().remove(item);
                    } else {
                        treeView.setRoot(null);
                    }

                break;
            default:
                break;
        }
        if(newItem != null) {
            if(item != null) {
                item.getChildren().add(newItem);
            } else {
               treeView.setRoot(newItem);
            }
        }

    }

    /**
     * Cancel Timer and exit
     */

    @Override
    public void stop() throws Exception {
        timerTask.cancel();
        super.stop();
        Platform.exit();
        System.exit(0);

    }



    @SneakyThrows
    /**
     * Main part of main Window initialization.
     * TreeViews, Buttons, Labels, Bindings are initialized
     */
    private Node addGridPane() {

        textFieldGet = new TextField(getUrl.getValue());
        getUrl.bind(textFieldGet.textProperty());
        treeView = new TreeView<>();
        treeViewPost = new TreeView();
        treeView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        textFieldPost = new TextField(postUrl.getValue());
        postUrl.bind(textFieldPost.textProperty());
        textFieldPost.setFont(Z5Configuration.Instance().getDefaultFont());
        treeViewPost.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);


       /* treeView.setCellFactory(new Callback<TreeView, TreeCell>() {
            @Override
            public TreeCell call(TreeView param) {
                return new JSONDragCellFactory();
            }
        });*/
        treeViewPost = new TreeView<Object>();
        treeViewPost.setMinSize(400,400);
        treeViewPost.setMaxSize(600,800);
        treeView.setMinSize(400,400);
        treeView.setMaxSize(600,800);

        textFieldGet.setFont(Z5Configuration.Instance().getDefaultFont());
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(2, 10, 0, 10));

        grid.add(textFieldGet, 1, 2);
        grid.add(textFieldPost, 4, 2);
        grid.add(treeView, 1, 6);
        HBox buttonBox = new HBox();
        VBox buttonStatusBox = new VBox();
        buttonBox.setAlignment(Pos.TOP_LEFT);
        Button button = new Button("Get");
        button.setFont(Z5Configuration.Instance().getDefaultFont());
       // grid.add(button, 0, 3);
        Button buttonPost = new Button("Post");
        buttonBox.getChildren().addAll(button, buttonPost);
        buttonPost.setFont(Z5Configuration.Instance().getDefaultFont());
        statusLabelGet = new StatusLabel("No request send.");
        buttonStatusBox.getChildren().addAll(statusLabelGet);
        buttonStatusBox.getChildren().addAll(buttonBox);
        buttonPost.setOnAction(createButtonEventHandler(HttpMethod.POST, treeView, getUrl, statusLabelGet ));
        button.setOnAction(createButtonEventHandler(HttpMethod.GET, treeView, getUrl, statusLabelGet));
        grid.add(buttonStatusBox, 1, 3);
        grid.add(treeViewPost, 4, 6);

        HBox buttonBoxPost = new HBox();
        VBox buttonStatusBoxPost = new VBox();
        buttonBoxPost.setAlignment(Pos.TOP_LEFT);
        Button buttonPostGet = new Button("Get");
        buttonPostGet.setFont(Z5Configuration.Instance().getDefaultFont());
        // grid.add(button, 0, 3);
        Button buttonPostPost = new Button("Post");
        buttonBoxPost.getChildren().addAll(buttonPostGet, buttonPostPost);
        buttonPostPost.setFont(Z5Configuration.Instance().getDefaultFont());
        statusLabelPost = new StatusLabel("No request send.");
        buttonStatusBoxPost.getChildren().addAll(statusLabelPost);
        buttonStatusBoxPost.getChildren().addAll(buttonBoxPost);
        buttonPostPost.setOnAction(createButtonEventHandler(HttpMethod.POST, treeViewPost, postUrl, statusLabelPost ));
        buttonPostGet.setOnAction(createButtonEventHandler(HttpMethod.GET, treeViewPost, postUrl, statusLabelPost));
        grid.add(buttonStatusBoxPost, 4, 3);
        return grid;
    }

    private EventHandler<ActionEvent> createButtonEventHandler(HttpMethod method, TreeView tv, SimpleStringProperty url, StatusLabel label) {
        EventHandler btnSolHandler = new EventHandler<Event>() {

            @SneakyThrows
            @Override
            public void handle(Event event) {
                doRequest(method, tv, url, method.equals(HttpMethod.POST) ? Optional.of(itemGenerator.treeItemToJson((TreeItem<Object>) tv.getSelectionModel().getSelectedItem())) : Optional.empty(), label);
            }
        };
        return btnSolHandler;
    }




    private void doRequest(HttpMethod method, TreeView treeView, SimpleStringProperty url, Optional<String> json, StatusLabel label) throws UnirestException, JsonProcessingException {
        String finalUrl = baseUrl.getValue() != null && !baseUrl.getValue().isEmpty() ? baseUrl.getValue()+url.getValueSafe() : url.getValueSafe();
        HttpRequest request =  method.equals(HttpMethod.POST) ?  Unirest.post(finalUrl) : new HttpRequest(method, finalUrl);
        ServerInformation loginInfos = loginDialog.getLoginInformation();
        String token = loginInfos.getCurrentToken().getValueSafe();
        String tokenHeader = loginInfos.getTokenHeaderProp().getValueSafe();
        String tokenPrefix = loginInfos.getTokenHeaderPrefix().getValueSafe();
        if(token != null && !token.isEmpty()) {
            if(!tokenPrefix.isEmpty()) {
                token = tokenPrefix + " " + token;
            }
            request.header(tokenHeader, token);
        }
        loginInfos.getAdditionalHeaders().entrySet().forEach(header -> {
            request.header(header.getKey(), header.getValue());
        });
        if(json.isPresent() && request instanceof HttpRequestWithBody) {
            HttpRequestWithBody bodyRequest = (HttpRequestWithBody) request;
            bodyRequest.body(json.get());
            //request.header("Content-type", "application/json");
        }

        HttpResponse<JsonNode> answer = request.asJson();
        log.info(answer.toString());
        label.setStatus(answer);
        Object values = null;
        try {
            values = mapper.readValue(answer.getBody().toString(), Object.class);
        }
        catch (Exception e) {
            log.warn("Json Answer not parseable");
            label.addStatus("\n---------------ANSWER----------\n" + answer.getBody().toString());
            values=answer.getBody().toString();
        }
        if(answer.getStatus() < 299) {
            // TreeItem<Object> rootNode = new TreeItem<Object>(new String(getUrl));
            TreeItem item = ((TreeItem<Object>) treeView.getSelectionModel().getSelectedItem());
            TreeItem newItem = itemGenerator.generateTreeItems(values, url.getValueSafe());
            if (item == null || item.getParent() == null) {
                treeView.setRoot(newItem);
            } else {
                TreeItem parent = item.getParent();
                String name = item.toString();
                parent.getChildren().remove(item);
                parent.getChildren().add(newItem);
            }
        }
    }

    @SneakyThrows
    private HttpClient noSSLVerificationClient() {
        SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy() {
            public boolean isTrusted(X509Certificate[] chain, String authType) {
                return true;
            }
        }).build();
        HttpClient customHttpClient = HttpClients.custom().setSSLContext(sslContext)
                .setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
        return customHttpClient;
    }

    /**
     * Headline Box
     * @return
     */
    @SneakyThrows
    private HBox addHBox(String color) {
        HBox hbox = new HBox();

        hbox.setPadding(new Insets(15, 12, 15, 12));
        hbox.setSpacing(30);
        hbox.setStyle("-fx-background-color: "+color);
        String appHeadLine = "";
        try {
            appHeadLine = (String) ((Z5Configuration)Z5Configuration.Instance()).get("zerofive.app.name").orElseGet(() -> "pohly_05 rulez");
        } catch (Exception e) {
           log.warn("headline couldn't be evaluated");
        }
        Label lbl = new Label(appHeadLine);
        lbl.setTextFill(Color.WHITESMOKE);
        lbl.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        hbox.getChildren().add(lbl);
        loginButton = new Button("Login");
        loginButton.setFont(Z5Configuration.Instance().getDefaultFont());
        loginButton.setBackground( BGDEFAULT);
        loginButton.setOnMouseReleased(event -> showLogin(event));
        loginButton.setAlignment(Pos.CENTER_RIGHT);
        hbox.getChildren().addAll(loginButton);




        return hbox;
    }

    private void showLogin(MouseEvent event) {
        loginDialog.showAndWait().ifPresent(result -> {
            if(result.booleanValue()) {  loginButton.setBackground(BGSUCESS); } else { loginButton.setBackground(BGDEFAULT); };
        });
    }


    public static void main(String[] args) {
        Z5Configuration config = null;
        try {
            config = Z5Configuration.Instance();
        } catch (Exception e) {
           log.error("Configuration not initialized.");
        }
        for(String arg : args) {
            if(arg.matches("-f.*") ) {
                String fileName = arg.substring(2);
                File file = new File(fileName);
                if(!file.exists()) {
                   file = new File(config.getStartUpPath().getAbsolutePath() + '/' + fileName);
                }
                    try {
                        config.addConfiguration(new FileInputStream(file));

                    } catch (FileNotFoundException e) {
                        log.warn("{} not Found. {}", file.getName(), e.getMessage());
                    }

            }
        }
        launch();
    }

}
