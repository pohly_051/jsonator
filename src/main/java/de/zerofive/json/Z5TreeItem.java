package de.zerofive.json;

import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

import java.util.List;

public interface Z5TreeItem<T> {
    boolean isMapEntry();
    boolean isList();
    boolean isMap();
    boolean isPrimitive();

    ObservableList<TreeItem<Object>> getChildren();

}
