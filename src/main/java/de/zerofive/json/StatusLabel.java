package de.zerofive.json;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import de.zerofive.core.Z5Configuration;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import lombok.Data;
import lombok.SneakyThrows;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class StatusLabel extends Label {


    private static Paint GREEN = Paint.valueOf("#22B14C");
    private static Paint RED = Paint.valueOf("#EB3324");
    private static Map<Integer, String> HTTPSTATUSMAP =  Arrays.stream(HttpStatus.class.getFields()).filter(field -> field.getType().isAssignableFrom(int.class) ).map(field -> {
        try {
            Object value = field.get(null);
            return value != null ? new AbstractMap.SimpleEntry<>(Integer.valueOf((int)value), field.getName().substring(3)) : null;
        } catch (IllegalAccessException | IndexOutOfBoundsException e) {
            e.printStackTrace();
            return null;
        }
    }).filter(field -> field != null).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
    private String lastMessage;
    @SneakyThrows
    public StatusLabel(String text) {
        super();
        setText(text);
        super.setOnMouseClicked(event -> handleClick(event));
        setMaxWidth(120);

        setFont(Z5Configuration.Instance().getDefaultFont());
    }

    private void handleClick(MouseEvent event) {
        if(this.lastMessage != null) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.getDialogPane().setMaxSize(600, 1000);
            alert.setTitle(getText());

            alert.setContentText(lastMessage);
            alert.getDialogPane().setMinSize(300, 500);

            alert.showAndWait();
        }
    }

    public void setStatus(HttpResponse<?> response) {

        if(response.getStatus() < 300) {
            setTextFill(GREEN);
        } else {
            setTextFill(RED);
        }

        // Easiest way to get a name
        StringBuilder messageBuilder = null;
       String responseStatus = null;
        if(HTTPSTATUSMAP.containsKey(response.getStatus())) {
            responseStatus = HTTPSTATUSMAP.get(response.getStatus());
            messageBuilder = new StringBuilder(responseStatus.length()+32);
            messageBuilder.append(response.getStatus()).append(" - ").append(responseStatus).append("\n");
        }
       //lastMessage = Arrays.stream(HttpStatus.class.getFields()).filter(field -> field.getType().isAssignableFrom(int.class) ).map(field -> {
       //    try {
       //        Object value = field.get(null);
       //        return value != null && (int)value == response.getStatus() ? field.getName() : null;
       //    } catch (IllegalAccessException e) {
       //        e.printStackTrace();
       //        return null;
       //    }
       //}).filter(field -> field != null).findFirst().get();
        if(responseStatus != null) {
            setText(String.valueOf(response.getStatus()) + " - " + responseStatus);
            if(response.getStatus() > 299) {

                messageBuilder.append("\n---------HEADERS---------\n");
                StringBuilder finalMessageBuilder = messageBuilder;
                response.getHeaders().forEach((k, v) -> {
                    finalMessageBuilder.append(k).append(": ").append(v).append("\n");
                });

                messageBuilder.append("\n---------BODY---------\n");
                messageBuilder.append(response.getBody().toString());

            }
            lastMessage = messageBuilder.toString();
        }


    }

    public void addStatus(String text) {
        StringBuilder stringBuilder = new StringBuilder(lastMessage.length()+text.length()+32);
        stringBuilder.append(lastMessage).append("\n").append(text);

    }
}
