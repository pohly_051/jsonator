package de.zerofive.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;
import de.zerofive.core.Z5Configuration;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import javafx.util.Callback;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static de.zerofive.json.Z5JSONConstants.*;

@Slf4j
/**
 * User Interface and logics.
 * Simple login to Endpoints by token.
 *
 */
public class LoginDialog extends Dialog<Boolean> {
    private GridPane root;

    // Avoid too many instances of Objectmapper - maybe would be better to implement in singleton
    private AtomicReference<ObjectMapper> mapper = new AtomicReference<ObjectMapper>(new ObjectMapper());
    private AtomicReference<String> lastLoginError = new AtomicReference<>();
    private ServerInformation serverInformation;
    private Label lblToken;
    private Label lblRefreshToken;
    private TextField userProp;
    private static Background BGGREY = new Background(new BackgroundFill(Paint.valueOf("#919191"), new CornerRadii(5), Insets.EMPTY));
    private static Background BGWHITE = new Background(new BackgroundFill(Paint.valueOf("#FFFFFF"), new CornerRadii(5), Insets.EMPTY));
    private StatusLabel loginStatusLabel = null;
    private StatusLabel refreshStatusLabel = null;

    private Boolean isLoggedIn = false;

    LoginDialog(Stage primaryStage, String name) {
        super();
        initOwner(primaryStage);
        this.setTitle(name);
        generatePane();

        var closeBtnType = new ButtonType("Close", ButtonBar.ButtonData.CANCEL_CLOSE);
        var loginButtonType = new ButtonType("Login", ButtonBar.ButtonData.APPLY);

        getDialogPane().getButtonTypes().addAll(closeBtnType, loginButtonType);
        getDialogPane().setContent(root);
        setResultConverter(new Callback<ButtonType, Boolean>() {
            @Override
            public Boolean call(ButtonType b) {

               return doLogin();
            }
        });
    }

    @SneakyThrows
    private void generatePane() {

        root= new GridPane();
        root.setHgap(10);
        root.setVgap(10);
        root.setPadding(new Insets(1, 5, 0, 5));
        Label label =new Label("Authentication Data");
        label.setFont(CONSOLAS_11);
        root.add(label, 0, 0);
        TableView headerTable = new TableView();

        /*TableColumn<Collection, String> loginColumn =
                new TableColumn("header");

        loginColumn.setCellValueFactory(cellData -> loginInformation.getAdditionalHeaders());
*/
        serverInformation = new ServerInformation();

        initConfig(Z5Configuration.Instance());
        serverInformation.getAdditionalHeaders().put("accept", "application/json");
        serverInformation.getAdditionalHeaders().put("Content-type", "application/json");
//headerTable.getColumns().addAll(loginColumn);
//headerTable.setItems(loginInformation.getAdditionalHeaders());
        TextField loginUrl = createNewTextField(serverInformation.getLoginUrl());
        Label lblLoginUrl = createLabelFor(loginUrl, "login url");

        TextField userName = createNewTextField(serverInformation.getUserName());
        Label lblUserName = createLabelFor(userName, "username");

        userProp = createNewTextField(serverInformation.getUserProp());
        /*userProp.setPromptText("username");
        userProp.setBackground(BGGREY);
        userProp.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
                   userProp.setBackground(BGWHITE);
                }
                else
                {
                   userProp.setBackground(BGGREY);
                }
            }
        });*/
        Label lblUserProp = createLabelFor(userProp, "users property (in json)");

        TextField pwProp = createNewTextField(serverInformation.getPwProp());
        Label lblPwProp = createLabelFor(pwProp, "password property (in json)");

        TextField pw = new PasswordField();
        //createNewTextField(loginInformation.getPwProp());
        pw.setFont(Z5Configuration.Instance().getDefaultFont());
        Label lblPw = createLabelFor(pw, "password");
        pw.setText(serverInformation.getPw().getValueSafe());
        serverInformation.getPw().bind(pw.textProperty());

        TextField tokenProp = createNewTextField(serverInformation.getTokenProp());
        Label lblTokProp = createLabelFor(tokenProp, "token property (in your login/refresh answer json)");
        lblToken = new Label();
        lblToken.setFont(Z5Configuration.Instance().getDefaultFont());
        lblToken.setMaxWidth(400);
        serverInformation.getCurrentToken().bind(lblToken.textProperty());
        TextField tokenRefreshProp = createNewTextField(serverInformation.getRefreshTokenProp());
        Label lblTokRefreshProp = createLabelFor(tokenRefreshProp, "token refresh property (in your login answer json)");
        lblRefreshToken = new Label();
        lblRefreshToken.setFont(Z5Configuration.Instance().getDefaultFont());
        lblRefreshToken.setMaxWidth(400);
        serverInformation.getCurrentRefreshToken().bind(lblRefreshToken.textProperty());

        TextField tokenRefreshURL = createNewTextField(serverInformation.getTokenRefreshUrl());
        Label lbltokRefreshUrl = createLabelFor(tokenRefreshURL, "token refresh URL");

        TextField tokenHeaderProp = createNewTextField(serverInformation.getTokenHeaderProp());
        Label lbltokHeaderProp = createLabelFor(tokenHeaderProp, "token header name");
        TextField tokenHeaderPrefix = createNewTextField(serverInformation.getTokenHeaderPrefix());
        Label lbltokHeaderPrefix = createLabelFor(tokenHeaderPrefix, "token header prefix");

        root.add(lblLoginUrl, 0, 1);
        root.add(loginUrl, 0, 2);
        root.add(lblUserProp, 0, 3);
        root.add(userProp, 0, 4);
        root.add(lblUserName, 1, 3);
        root.add(userName, 1, 4);
        root.add(lblPwProp, 0, 5);
        root.add(pwProp, 0,6);
        root.add(lblPw, 1, 5);
        root.add(pw, 1,6);
        loginStatusLabel = new StatusLabel("no Login");
        root.add(loginStatusLabel, 0, 7);

        root.add(lblTokProp, 0, 8);
        root.add(tokenProp, 0, 9);
        root.add(lblToken, 1, 9);
        root.add(lblTokRefreshProp, 0, 10);

        root.add(tokenRefreshProp, 0, 11);
        root.add(lblRefreshToken, 1, 11);
        root.add(lbltokRefreshUrl, 0, 12);
        root.add(tokenRefreshURL, 0, 13);
        root.add(lbltokHeaderProp, 0, 14);
        root.add(tokenHeaderProp, 0, 15);
        root.add(lbltokHeaderPrefix, 1, 14);
        root.add(tokenHeaderPrefix, 1, 15);
        refreshStatusLabel = new StatusLabel("no token Refresh");
        root.add(refreshStatusLabel, 0, 16);
        Button resetLoginButton = new Button("Reset Login");
        resetLoginButton.setOnAction(event -> login());
        root.add(resetLoginButton, 0, 17);
        login();

    }

    private void initConfig(Z5Configuration configuration) {
        serverInformation.getLoginUrl().setValue((String) configuration.get("zerofive.json.auth.loginURL").orElse(null));
        serverInformation.getUserProp().setValue((String)configuration.get("zerofive.json.auth.userProp").orElse(null));
        serverInformation.getPwProp().setValue((String)configuration.get("zerofive.json.auth.pwProp").orElse(null));
        serverInformation.getUserName().setValue((String)configuration.get("zerofive.json.auth.userName").orElse(null));
        serverInformation.getPw().setValue((String)configuration.get("zerofive.json.auth.pw").orElse(null));
        serverInformation.getTokenRefreshUrl().setValue((String)configuration.get("zerofive.json.auth.refreshTokenURL").orElse(null));
        serverInformation.getRefreshTokenProp().setValue((String) configuration.get("zerofive.json.auth.refreshTokenProp").orElse(null));
        serverInformation.getTokenHeaderProp().setValue((String)configuration.get("zerofive.json.auth.headerTokenProp").orElse(null));
        serverInformation.getTokenProp().setValue((String)configuration.get("zerofive.json.auth.tokenProp").orElse(null));
        serverInformation.getTokenHeaderPrefix().setValue((String)configuration.get("zerofive.json.auth.headerTokenPrefix").orElse(null));
        serverInformation.getAdditionalHeaders().putAll(((Map) configuration.get("zerofive.json.auth.additionalHeaders").orElse(new LinkedHashMap<>())));

    }

    public boolean doLogin() {
        boolean attemptSuceeded = false;
       String value = serverInformation.getCurrentToken().get();
        if(value != null && !value.isEmpty()) {
            attemptSuceeded = refreshToken();
            if(!attemptSuceeded) {
                attemptSuceeded = login();
            }
        } else {
            attemptSuceeded = login();
        }
        return attemptSuceeded && serverInformation.getCurrentToken() != null;
    }

    private boolean login() {
        boolean attemptSuceeded = true;
        Object tokenValues = null;
        String url = serverInformation.getLoginUrl().getValueSafe();
        if(url != null && !url.isEmpty()) {
            try {
                HttpResponse<JsonNode> answer =null;
                HttpRequestWithBody request = Unirest.post(url);
                serverInformation.getAdditionalHeaders().entrySet().forEach(entry -> request.header(entry.getKey(), entry.getValue()));
                request.header("Content-type", "application/json");
                answer = request.body(createLoginJSON()).asJson();
                loginStatusLabel.setStatus(answer);
                log.info(answer.toString());
                tokenValues = mapper.get().readValue(answer.getBody().toString(), Object.class);
                attemptSuceeded = parseTokenAnswer(tokenValues);
                //attemptSuceeded = refreshToken();

            } catch (JsonProcessingException | UnirestException e) {
                log.error("Error while trying to login. {}", e.getMessage());
                lastLoginError.getAndSet(e.getMessage());
                attemptSuceeded = false;
            }
        }
        return attemptSuceeded;

    }

    private boolean parseTokenAnswer(Object tokenValues) {
        String tokenProp = serverInformation.getTokenProp().getValueSafe();
        String refreshTokenProp = serverInformation.getRefreshTokenProp().getValueSafe();
        if(tokenValues instanceof Map ) {
            Map tokenMap = (Map)tokenValues;
            if(!tokenMap.containsKey(tokenProp)) {
                log.error("Your token property {} was not found in the answer.", tokenProp);
                return false;
            } else {
                lblToken.setText((String) tokenMap.get(tokenProp));
                if(!tokenMap.containsKey(refreshTokenProp)) {
                    log.warn("Your refreshToken property was not found in the answer.", refreshTokenProp);
                } else {
                    lblRefreshToken.setText((String) tokenMap.get(refreshTokenProp));
                }
            }
        }
        return true;
    }

    private String createLoginJSON() {
        StringBuilder loginJson = new StringBuilder(100);
        loginJson.append("{\"");
        loginJson.append(serverInformation.getUserProp().getValueSafe()).append("\":\"");
        loginJson.append(serverInformation.getUserName().getValueSafe()).append("\",\"");
        loginJson.append(serverInformation.getPwProp().getValueSafe()).append("\":\"");
        loginJson.append(serverInformation.getPw().getValueSafe()).append(("\"}"));
        return loginJson.toString();
    }

    private boolean refreshToken() {
        boolean attemptSuccessfull = false;
        String url = serverInformation.getTokenRefreshUrl().getValue();
       Object tokenValues = null;
        HttpResponse<JsonNode> response = null;
        if(url != null && !url.isEmpty()) {
            try {
                HttpRequestWithBody request = Unirest.post(url);
                serverInformation.getAdditionalHeaders().entrySet().forEach(entry -> request.header(entry.getKey(), entry.getValue()));
                String tokenHeader = serverInformation.getTokenHeaderProp().getValueSafe();
                String tokenPrefix = serverInformation.getTokenHeaderPrefix().getValueSafe();
                String refreshToken = serverInformation.getCurrentRefreshToken().getValueSafe();
                if(refreshToken != null && !refreshToken.isEmpty()) {
                    if(!tokenPrefix.isEmpty()) {
                        refreshToken = tokenPrefix + " " + refreshToken;
                    }
                    request.header(tokenHeader, refreshToken);
                }
                response = request.asJson();
                refreshStatusLabel.setStatus(response);
                tokenValues = mapper.get().readValue(response.getBody().toString(), Object.class);

                parseTokenAnswer(tokenValues);
                log.info(response.toString());
                attemptSuccessfull = true;
            } catch (JsonProcessingException | UnirestException e) {
                log.error("Failure while trying to refresh token.");
                attemptSuccessfull = false;
                lastLoginError.getAndSet(e.getMessage());
            }
        }
        return attemptSuccessfull;
    }

    private String createRefreshJSON() {
        StringBuilder refreshJson = new StringBuilder(100);
        refreshJson.append("{\"");
        refreshJson.append(serverInformation.getRefreshTokenProp().getValueSafe()).append("\":\"");
        refreshJson.append(serverInformation.getCurrentRefreshToken().getValueSafe()).append("\"}");
        return refreshJson.toString();
    }

    private Label createLabelFor(TextField tf, String text) throws Exception {
        Label label =new Label(text);
        label.setFont(Z5Configuration.Instance().getDefaultFont());
        label.setLabelFor((tf));
        return label;
    }

    private TextField createNewTextField(SimpleStringProperty userProp) throws Exception {
        TextField tf = new TextField();
        tf.setFont(Z5Configuration.Instance().getDefaultFont());
        //tf.textProperty().bind(userProp);
        // Init property from future binding
        tf.setText(userProp.getValueSafe());
        // and bind
        //tf.textProperty().bind(userProp);
        userProp.bind(tf.textProperty());
       //
        tf.setPrefWidth(400);
        tf.setMaxWidth(400);
        return tf;
    }

    public ServerInformation getLoginInformation() {
        return serverInformation;
   }



}
