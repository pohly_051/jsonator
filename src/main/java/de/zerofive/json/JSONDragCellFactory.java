package de.zerofive.json;

import de.zerofive.core.Z5Configuration;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.*;
import javafx.util.Callback;
import lombok.SneakyThrows;

import java.util.LinkedHashMap;
import java.util.Map;

import static de.zerofive.json.Z5JSONConstants.CONSOLAS_11;

/**
 * Cell Factory for trees
 * with the ability to
 * drag JSON nodes
 */
public class JSONDragCellFactory implements Callback<TreeView<Object>, TreeCell<Object>>{
    /**
     * Default Constructor
     */
    public JSONDragCellFactory() {
        super();

    }




    @SneakyThrows
    private void handleDragDetected(MouseEvent event, TreeCell cell) {

        TreeItem draggedItem = cell.getTreeItem();

        Dragboard db = cell.startDragAndDrop(TransferMode.COPY);
       JSONTreeItemGenerator generator = JSONTreeItemGenerator.Instance();
        ClipboardContent content = new ClipboardContent();
        Z5TreeItem z5TreeItem = (Z5TreeItem)draggedItem;
        content.putString((z5TreeItem.isMapEntry() ? "C" : "F") + "@" + draggedItem.getValue().toString() + "$@" + generator.treeItemToJson(draggedItem));
        db.setContent(content);
        db.setDragView(cell.snapshot(null, null));

        event.consume();
    }

    @Override
    public TreeCell call(TreeView<Object> treeView) {
        TreeCell<Object> cell = new TreeCell<Object>() {
            @SneakyThrows
            @Override
            protected void updateItem(Object item, boolean empty) {

                super.updateItem(item, empty);
                // setText(null) is neccessary because treeViews
                // reuse initialized cell Objects
                if (item == null) {setText(null); return;}
                setFont(Z5Configuration.Instance().getDefaultFont());
                // set item
                setText(String.valueOf(item));
            }
        };
        cell.setOnDragDetected((MouseEvent event) -> handleDragDetected(event, cell));


        return cell;
    }
}
