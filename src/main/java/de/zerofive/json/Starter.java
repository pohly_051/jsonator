package de.zerofive.json;

import de.zerofive.core.Z5Configuration;

import java.lang.reflect.InvocationTargetException;

/**
 * Helper class to start the app.
 */
public class Starter {

        public static void main(final String[] args) throws Exception {
            Z5Configuration.Instance();

            App.main(args);
        }

}

