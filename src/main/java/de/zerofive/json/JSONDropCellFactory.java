package de.zerofive.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.zerofive.core.*;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Pair;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static de.zerofive.json.Z5JSONConstants.CONSOLAS_11;

/**
 * Cell Factory for trees
 * with the ability to
 * drop JSON nodes and files
 */
@Slf4j
public class JSONDropCellFactory implements Callback<TreeView<Object>, TreeCell<Object>> {
    private JSONEditDialog editDialog;
    public JSONDropCellFactory(Stage primaryStage) {
        super();
        editDialog = new JSONEditDialog(primaryStage, "");


    }


    @SneakyThrows
    private void handleDragOver(DragEvent event, TreeCell cell) {
        if (!event.getDragboard().hasString() && !event.getDragboard().hasFiles()) return;
        TreeItem thisItem = cell.getTreeItem();
       // JSONTreeItemGenerator generator = JSONTreeItemGenerator.Instance();
        //TreeItem draggedItem = generator.jsonToTre{"geraete":[],"name":"Miri","position":[["1","2"],["1","2"],["5","6"],["10","10"],["6","1"]],"alter":1}eItems(event.getDragboard().getString());
        // can't drop on itself{"geraete":[],"name":"Miri","position":[["1","2"],["1","2"],["5","6"],["10","10"],["6","1"]],"alter":1}
        //if (draggedItem == null || thisItem == null || thisItem == draggedItem) return;
        // ignore if this is the root

        event.acceptTransferModes(TransferMode.COPY);

        //dropZone.setStyle(DROP_HINT_STYLE);

    }

    @SneakyThrows
    private void handleDropped(DragEvent event, TreeCell cell) {
        Dragboard db = event.getDragboard();
        boolean success = true;

        if (db.getString() == null && db.getFiles() == null) return;

        JSONTreeItemGenerator generator = JSONTreeItemGenerator.Instance();

        Z5TreeItem thisItem =(Z5TreeItem)cell.getTreeItem();
        TreeItem draggedItem = null;

        TreeView treeView = cell.getTreeView();
        boolean children = false;
        if (db.getString() != null && !db.getString().startsWith("file:/")) {
            int startJson = db.getString().indexOf("$@");
            children = db.getString().charAt(0) == 'C' ? true : false;
            String name = db.getString().substring(1, startJson).replace("@", "").replace("$", "");
            String json = db.getString().substring(startJson+2);
            draggedItem = (TreeItem) generator.jsonToTreeItems(json, name);

        }
        else {

            File droppedFile = db.getFiles() != null ? db.getFiles().get(0) : new File(db.getString().replace("\r", "").replace("\n", "").replace("file:/", ""));

            if(!droppedFile.exists()) {
                log.error("Cannot find dropped File {}", droppedFile.getAbsolutePath());
                event.setDropCompleted(false);
                return;
            }
            try {
                draggedItem = generator.generateTreeItems(droppedFile);
            }
            catch (JsonProcessingException e) {
                draggedItem = new TreeItem<>("Error parsing JSON");
                log.error(e.getMessage());
                success = false;
            }
        }

        List<TreeItem> removeItems = new ArrayList<>();
        if (thisItem != null) {

            if(!children) {
                TreeItem newItem = (TreeItem) draggedItem;
                for (Object existing : thisItem.getChildren()) {
                    TreeItem exItem = (TreeItem) existing;
                    if (exItem.getValue().equals(newItem.getValue())) {
                        removeItems.add(exItem);
                    } else if (exItem.getValue() instanceof Pair) {
                        try {
                            Pair<String, Object> exPair = (Pair) exItem.getValue();
                            Pair<String, Object> newPair = (Pair) newItem.getValue();
                            if (exPair.getKey().equals(newPair.getKey())) {
                                removeItems.add(exItem);
                            }
                        } catch (ClassCastException e) {
                            log.error("Trying to replace different data types");
                        }
                    }
                }
            } else {
                removeItems.addAll((List<TreeItem>) draggedItem.getChildren().stream().map(item -> {
                    TreeItem newItem = (TreeItem) item;
                    for(Object existing : thisItem.getChildren()) {
                        TreeItem exItem = (TreeItem)existing;
                        if(exItem.getValue().equals(newItem.getValue())) {
                            return exItem;
                        } else if(exItem.getValue() instanceof Pair ) {
                            try {
                                Pair<String, Object> exPair = (Pair)exItem.getValue();
                                Pair<String, Object> newPair = (Pair)newItem.getValue();
                                if(exPair.getKey().equals(newPair.getKey())) {
                                    return exItem;
                                }
                            }
                            catch (ClassCastException e) {
                                log.error("Trying to replace different data types");
                            }
                        }
                    }
                    return null;
                }).filter(item -> item != null).collect(Collectors.toList()));
            }

            Z5TreeItem z5TreeItem = (Z5TreeItem)thisItem;

            if(!draggedItem.isLeaf() /*&& !((Z5TreeItem)draggedItem).isList()*/) {
                if(!z5TreeItem.isList()) {
                    thisItem.getChildren().removeAll(removeItems);
                    if(children) {
                        thisItem.getChildren().addAll(draggedItem.getChildren());
                    }
                    else  {
                        thisItem.getChildren().addAll(draggedItem);
                    }
                }  else {
                    thisItem.getChildren().add(draggedItem);
                }
                draggedItem = null;
            }
            else {
                thisItem.getChildren().add(draggedItem);
            }
        } else {
            treeView.setRoot(draggedItem);

        }
        treeView.getSelectionModel().select(draggedItem != null ? draggedItem : thisItem);
        event.setDropCompleted(success);
    }


    @Override
    public TreeCell<Object> call(TreeView<Object> param) {
        TreeCell<Object> cell = new TreeCell<Object>() {
            @SneakyThrows
            @Override
            protected void updateItem(Object item, boolean empty) {

                super.updateItem(item, empty);
                if (item == null) {setText(null); return;}
                setFont(Z5Configuration.Instance().getDefaultFont());
                setText(String.valueOf(item));
            }
        };

       // cell.setOnDragDetected((MouseEvent event) -> handleDragDetected(event, cell));
        cell.setOnDragOver(event -> handleDragOver(event, cell));
        cell.setOnDragDropped(event -> handleDropped(event, cell));
        cell.setOnMouseClicked(event -> handleMouseClicks(event, cell));

        return cell;
    }



    @SneakyThrows
    private void handleMouseClicks(MouseEvent event, TreeCell<Object> cell) {
        JSONTreeItemGenerator generator = JSONTreeItemGenerator.Instance();
        TreeItem item = cell.getTreeItem();
        if(event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2) {
            String json = generator.treeItemToJson(cell.getTreeItem(), true);
            AtomicReference<TreeItem> newElement = new AtomicReference<>();
            editDialog.setJson(json);
            loopEditDialog(newElement, item);
            if(newElement.get() != null) {
                TreeItem parent = item != null ? item.getParent() : null;
                if(parent != null) {
                    Z5TreeItem z5Parent = (Z5TreeItem)parent;

                    Z5TreeItem z5Item = (Z5TreeItem) item;
                    if(z5Parent.isList()) {
                       // TreeItem newParent = newElement.get().getClass().getDeclaredConstructor(String.class).newInstance(String.valueOf(parent.getChildren().size()));
                       // newParent.getChildren().add(newElement.get());
                        parent.getChildren().add(newElement.get());
                    } else {
                        if(!((Z5TreeItem)newElement.get()).isList()) {
                            if(z5Item.isMapEntry()) {
                                parent.getChildren().addAll(newElement.get().getChildren());
                            } else {
                                parent.getChildren().add(newElement.get());
                            }
                        } else {
                            parent.getChildren().add(newElement.get());
                        }
                    }
                    parent.getChildren().remove(item);
                } else {
                    cell.getTreeView().setRoot(newElement.get());
                }
            }
        }
        event.consume();
    }

    private void loopEditDialog(AtomicReference<TreeItem> newElement, TreeItem item) throws Exception {

        JSONTreeItemGenerator generator = JSONTreeItemGenerator.Instance();
        editDialog.showAndWait().ifPresent(result -> {
            try {
                newElement.set(generator.jsonToTreeItems(result, String.valueOf(item != null && item.getValue() != null ? item.getValue() : "root")));
            } catch (JsonProcessingException e) {
                // Alert alert = new Alert(Alert.AlertType.ERROR);
                StringBuilder message = new StringBuilder(80);
                message.append("JSON unparsable\n").append(e.getMessage());
                Optional<ButtonType> decResult = showDecisionAlert();
                if(decResult.get().getButtonData() == ButtonBar.ButtonData.OK_DONE) {
                    try {
                        loopEditDialog(newElement, item);
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                } else if(decResult.get().getButtonData() == ButtonBar.ButtonData.HELP) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, message.toString());
                    alert.showAndWait();
                    try {
                        loopEditDialog(newElement, item);
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }

            }
        });
    }

    private Optional<ButtonType> showDecisionAlert() {
        ButtonType btnAgain = new ButtonType("Try to fix", ButtonBar.ButtonData.OK_DONE);
        ButtonType btnCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        ButtonType btnInfo = new ButtonType("Show Message", ButtonBar.ButtonData.HELP);
        Alert alert = new Alert(Alert.AlertType.WARNING,
                "JSON is invalid. What do you want to do?",
                btnAgain,
                btnCancel,
                btnInfo);

        Optional<ButtonType> result = alert.showAndWait();

       return result;
    }


}
