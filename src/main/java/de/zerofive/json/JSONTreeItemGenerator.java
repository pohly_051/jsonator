package de.zerofive.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import de.zerofive.core.ListTreeItem;
import de.zerofive.core.MapEntryTreeItem;
import de.zerofive.core.MapTreeItem;
import de.zerofive.core.PrimitiveTreeItem;
import javafx.beans.property.SimpleMapProperty;
import javafx.scene.control.TreeItem;
import javafx.util.Pair;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * Singleton generates json from tree or tree from json
 */
@Slf4j
public class JSONTreeItemGenerator {
    private AtomicReference<ObjectMapper> mapper = new AtomicReference<ObjectMapper>(new ObjectMapper());
    private static AtomicReference<JSONTreeItemGenerator> INSTANCE = new AtomicReference<>();
    private static String DEFAULTFILE = "app.yml";

    /**
     * Get the Singletons instance
     * @return
     * @throws Exception
     */
    public static JSONTreeItemGenerator Instance() throws Exception {
        if (INSTANCE.get() == null) {
            INSTANCE.getAndSet(new JSONTreeItemGenerator());
        }
        return INSTANCE.get();
    }

    public JSONTreeItemGenerator() {


    }

    /**
     * Generates a treeitem from a JSON
     * @param value the root Object
     * @param name tree objects root name (if needed)
     * @return the treeitem
     */
    public TreeItem<Object> generateTreeItems(Object value, String name) {
        TreeItem<Object> item = null;
        AtomicInteger count = new AtomicInteger();
        if (value instanceof Collection) {
            item = new ListTreeItem(name);
            item.getChildren().addAll(
                    (Collection<? extends TreeItem<Object>>) ((Collection) value).stream().map(currItem -> {
                        return generateTreeItems(currItem, String.valueOf(count.getAndIncrement()));
                    }).collect(Collectors.toList()));
        } else if (value instanceof Map) {
            item = new MapTreeItem(name);
            item.getChildren().addAll((Collection<? extends TreeItem<Object>>) ((Map) value).entrySet().stream().map(entry -> {
                return generateTreeItems(entry, String.valueOf(count.getAndIncrement()));
            }).collect(Collectors.toList()));
        } else if (value instanceof Map.Entry) {
            Map.Entry<Object, Object> entry = (Map.Entry) value;
            if (!(entry.getValue() instanceof Collection || entry.getValue() instanceof Map)) {
                item = new MapEntryTreeItem(new Pair<String, Object>((String) entry.getKey(), entry.getValue()));
            } else {
                item = generateTreeItems(entry.getValue(), (String) entry.getKey());
            }

        } else {
                item = new PrimitiveTreeItem(value);

        }
        return item;

    }

    /**
     * Generate root JSON serializable Object from treeItem (and its parent)
     * @param item The item to serialize
     * @return serializable Object
     * @throws NoSuchMethodException
     */
    public Object treeItemToSerializable(TreeItem<Object> item) throws NoSuchMethodException {
        if(item == null) return  null;
        Object returnValue = null;
        Z5TreeItem myItem = (Z5TreeItem) item;
        if (myItem.isMapEntry()) {

            returnValue = new AbstractMap.SimpleEntry<>(((Pair)item.getValue()).getKey(), ((Pair)item.getValue()).getValue());

        } else if(myItem.isPrimitive()) {

            returnValue = item.getValue();
        } else {
            if (myItem.isList()) {
                returnValue = new ArrayList<>();
                List casted = (List) returnValue;
                casted.addAll(item.getChildren().stream().map(child -> {
                    try {
                        return treeItemToSerializable(child);
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                    return null;
                }).collect(Collectors.toList()));
            } else if (myItem.isMap()) {
                returnValue = new LinkedHashMap<>();
                Map casted = (Map) returnValue;
                item.getChildren().stream().forEach(child -> {
                    try {
                        Z5TreeItem castedChild = (Z5TreeItem) child;
                        Map.Entry<Object, Object> mapEntry = null;
                        Object serialized = treeItemToSerializable(child);
                        try {
                            mapEntry = (Map.Entry<Object, Object>) serialized;
                        } catch (ClassCastException e) {
                            if (castedChild.isList() || castedChild.isPrimitive() || castedChild.isMap()) {
                                mapEntry = new AbstractMap.SimpleEntry<Object, Object>( child.getValue().getClass().getDeclaredConstructor(child.getValue().getClass()).newInstance(child.getValue()), serialized);
                            }
                        }
                        casted.put(mapEntry.getKey(), mapEntry.getValue());
                    } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                });
            }
        }
        return returnValue;
    }

    /**
     * Generates json String from treeItem.
     * @param item TreeItem to convert
     * @return json representation of the tree
     * @throws NoSuchMethodException
     * @throws JsonProcessingException
     */
    public String treeItemToJson(TreeItem item) throws NoSuchMethodException, JsonProcessingException {
       return  treeItemToJson(item, false);
    }

    /**
     * Generates json String from treeItem.
     * @param item TreeItem to convert
     * @param pretty pretty printer?
     * @return json representation of the tree
     * @throws NoSuchMethodException
     * @throws JsonProcessingException
     */
    public String treeItemToJson(TreeItem item, boolean pretty) throws NoSuchMethodException, JsonProcessingException {

       ObjectWriter writer = !pretty ? mapper.get().writer() : mapper.get().writerWithDefaultPrettyPrinter();
        Object obj = treeItemToSerializable(item);

        String json = obj != null ? writer.writeValueAsString(obj) : "{}";
        return json;
    }

    /**
     * Generate TreeItem from JSON.
     * @param string json to convert
     * @param name root Node name (if needed)
     * @return TreeItem generated item
     * @throws JsonProcessingException
     */
    public TreeItem<Object> jsonToTreeItems(String string, String name) throws JsonProcessingException {
        Object obj = mapper.get().readValue(string, Object.class);
        return generateTreeItems(obj, name);
    }

    /**
     * Generate TreeItem from JSON file.
     * @param inputFile
     * @return TreeItem generated item
     * @throws IOException
     */
    public TreeItem<Object> generateTreeItems(File inputFile) throws IOException {
        Object obj = mapper.get().readValue(inputFile, Object.class);
        return generateTreeItems(obj, inputFile.getName());
    }

    /**
     * The Jackson objectmapper.
     * @return ObjectMapper mapper
     */
    public ObjectMapper getMapper() {
        return mapper.get();
    }
}





