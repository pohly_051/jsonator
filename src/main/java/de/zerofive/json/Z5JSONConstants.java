package de.zerofive.json;

import javafx.scene.text.Font;

/**
 * Constants
 */
public class Z5JSONConstants {
    public static Font CONSOLAS_13 = Font.font("Consolas", 13);
    public static Font CONSOLAS_15 = Font.font("Consolas", 15);
    public static Font CONSOLAS_11 = Font.font("Consolas", 12);
}
