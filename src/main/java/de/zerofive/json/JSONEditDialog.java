package de.zerofive.json;

import de.zerofive.core.Z5Configuration;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import lombok.SneakyThrows;

import static de.zerofive.json.Z5JSONConstants.*;

/**
 * Dialog to edit JSON nodes
 */
public class JSONEditDialog extends Dialog<String> {
    private String json;
    private Button btn_save;
    private BorderPane root;
    private TextArea area;

    /**
     * Constructor
     * @param primaryStage the primary stage
     * @param name dialog name
     */
    JSONEditDialog(Stage primaryStage, String name) {
        super();
        initOwner(primaryStage);
        this.setTitle(name);
        generatePane();

        var closeBtnType = new ButtonType("Close", ButtonBar.ButtonData.CANCEL_CLOSE);
        getDialogPane().getButtonTypes().addAll(closeBtnType);
        getDialogPane().setContent(root);
        setResultConverter(new Callback<ButtonType, String>() {
            @Override
            public String call(ButtonType b) {

               return area.getText();
            }
        });
    }

    @SneakyThrows
    /**
     * Generates the pane
     */
    private void generatePane() {
        root= new BorderPane();
        Label label =new Label("jsonator edit");
        label.setFont(Z5Configuration.Instance().getDefaultFont());
        root.setTop(label);
        area = new TextArea();
        area.setMinHeight(300);
        area.setMaxSize(1024, 1024);
        this.getDialogPane().setMaxSize(1200, 1200);
        this.setResizable(true);
        area.setFont(Z5Configuration.Instance().getDefaultFont());

        root.setCenter(area);
    }

    /**
     * Set the dialogs json text.
     * @param json text for dialog
     */
    public void setJson(String json) {
        area.setText(json);
    }



}
