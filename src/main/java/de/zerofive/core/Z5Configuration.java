package de.zerofive.core;

import javafx.scene.text.Font;
import lombok.Data;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static de.zerofive.json.Z5JSONConstants.CONSOLAS_11;

/**
 * Singleton Class to init and Keep configuration
 * properties from yaml file.
 */
@Slf4j
public class Z5Configuration  {
    private AtomicReference<Map<String, Object>> configuration;
    private static volatile AtomicReference<Z5Configuration> INSTANCE = new AtomicReference<>();
    private static String DEFAULTFILE = "app.yml";
    private AtomicReference<Font> defaultFont;
    private File startUpPath = null;


    /**
     * Instance of this
     * @return Z5Configuration Object
     * @throws Exception on Error
     */
    public static Z5Configuration Instance() throws Exception {
        if(INSTANCE.get() == null) {
            INSTANCE.compareAndSet(null, new Z5Configuration());
        }
        return INSTANCE.get();
    }
    private Z5Configuration() {
        this.configuration = new AtomicReference<>();
        this.defaultFont = new AtomicReference<>(CONSOLAS_11);
        this.startUpPath = new java.io.File(".");
        addConfiguration(this.getClass().getClassLoader()
                .getResourceAsStream(DEFAULTFILE));
    }

    /**
     * Adds a new Configuration
     * and merges it with the current
     * if any
     * @param inputStream Stream to read
     */
    public void addConfiguration(InputStream inputStream) {

        Yaml yaml = new Yaml();
        try {

            Map<String, Object> currentMap = yaml.load(inputStream);
            if (configuration.get() == null) {
                if (!configuration.compareAndSet(null, currentMap)) {
                    configuration.get().putAll(currentMap);
                }
            } else {
                //configuration.get().putAll(currentMap);
                deepMerge(configuration.get(), currentMap);
            }

        } catch (Exception e) {
            log.warn("no/wrong config");
        }
        try {
            inputStream.close();
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    private static Map deepMerge(Map original, Map newMap) {
        for (Object key : newMap.keySet()) {
            if (newMap.get(key) instanceof Map && original.get(key) instanceof Map) {
                Map originalChild = (Map) original.get(key);
                Map newChild = (Map) newMap.get(key);
                original.put(key, deepMerge(originalChild, newChild));
            } else if (newMap.get(key) instanceof List && original.get(key) instanceof List) {
                List originalChild = (List) original.get(key);
                List newChild = (List) newMap.get(key);
                for (Object each : newChild) {
                    if (!originalChild.contains(each)) {
                        originalChild.add(each);
                    }
                }
            } else {
                original.put(key, newMap.get(key));
            }
        }
        return original;
    }

    /**
     * Get property by simple path
     *
     * @param property the prop to find e.g. zerofive.core.app
     * @return Optional of Object
     */
    public Optional<Object> get(String property) {
        Map currentMapElement = configuration.get();
        Object next = null;
        for(String prop : property.split("\\.")) {
            if(!currentMapElement.containsKey(prop)) {
                next = null;
                 break;
            }
                 next = currentMapElement.get(prop);
                if(next instanceof Map) {
                    currentMapElement = (Map)next;
                }
         }

            return  next != null ? Optional.of(next) : Optional.empty();
        }


    /**
     * Default font
     * @return the default font for the app.
     */
    public Font getDefaultFont() {
        return defaultFont.get();
    }

    /**
     * Applications Startup path
     */
    public File getStartUpPath() { return startUpPath;}

}
