package de.zerofive.core;

import de.zerofive.json.Z5TreeItem;
import javafx.scene.control.TreeItem;

/**
 * TreeItem Implementation for Map Nodes.
 */
public class MapTreeItem extends TreeItem<Object> implements Z5TreeItem<Object> {
    public MapTreeItem(String name) {
        super(name);
    }

    @Override
    public boolean isMapEntry() {
        return false;
    }

    @Override
    public boolean isList() {
        return false;
    }

    @Override
    public boolean isMap() {
        return true;
    }

    @Override
    public boolean isPrimitive() {
        return false;
    }
}
