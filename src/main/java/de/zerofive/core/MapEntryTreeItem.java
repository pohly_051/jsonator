package de.zerofive.core;

import de.zerofive.json.Z5TreeItem;
import javafx.scene.control.TreeItem;
import javafx.util.Pair;

/**
 * TreeItem Implementation for Map entry Nodes.
 */
public class MapEntryTreeItem extends TreeItem<Object> implements Z5TreeItem {
    public MapEntryTreeItem(Pair<String, Object> stringObjectPair) {
        super(stringObjectPair);
    }

    @Override
    public boolean isMapEntry() {
        return true;
    }

    @Override
    public boolean isList() {
        return false;
    }

    @Override
    public boolean isMap() {
        return false;
    }

    @Override
    public boolean isPrimitive() {
        return false;
    }
}
