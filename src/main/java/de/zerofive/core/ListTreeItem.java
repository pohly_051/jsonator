package de.zerofive.core;

import de.zerofive.json.Z5TreeItem;
import javafx.scene.control.TreeItem;

/**
 * TreeItem Implementation for List Nodes.
 */
public class ListTreeItem extends TreeItem<Object> implements Z5TreeItem<Object> {
    public ListTreeItem(String name) {
        super(name);
    }

    @Override
    public boolean isMapEntry() {
        return false;
    }

    @Override
    public boolean isList() {
        return true;
    }

    @Override
    public boolean isMap() {
        return false;
    }

    @Override
    public boolean isPrimitive() {
        return false;
    }
}
