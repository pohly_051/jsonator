package de.zerofive.core;

import de.zerofive.json.Z5TreeItem;
import javafx.scene.control.TreeItem;

/**
 * TreeItem Implementation for primitive Nodes (like int, String, etc).
 */
public class PrimitiveTreeItem extends TreeItem<Object> implements Z5TreeItem {
    public PrimitiveTreeItem(Object s) {
        super(s);
    }

    @Override
    public boolean isMapEntry() {
        return false;
    }

    @Override
    public boolean isList() {
        return false;
    }

    @Override
    public boolean isMap() {
        return false;
    }

    @Override
    public boolean isPrimitive() {
        return true;
    }
}
