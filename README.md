# jsonator

jsonator is a simple json edit and REST client tool.
<br/>
It's a fun project developed by me.

# Main features
- You can use jsonators left tree (treeView in Code) to GET your data
from a REST Server and drag the objects to the right.<br>
- The results can be dropped to the right tree (treeViewDrop in code), where
you can modify or expand your json tree by editing the json itself or dropping more elements.
- The left tree could be called GET-tree but you can even do a post from it.<br>
But: You can only GET information here.<br>
- The right tree can be called POST-tree. Here you can modify your JSON. Load data (by GET, POST or dropping a JSON File).<br>
In the end you can POST every node you modified to your server.
  
# developer tool
- Feel free to use, modify or distribute jsonator.<br>
- Contributing your changes would be very likely. Please publish your changes or send a request to me. ;)<br>
- Use jsonator on our own risk. I'm not responsible for any damages you made with my tool.<br>

# options and yml
- zerofive.app.name (not in GUI)<br>
  - Names your app. A white smoke Headline is written on dark green background.<br>
- zerofive.json<br>
  - baseURL (not in GUI)<br>
    - Could be left empty, then you have to define the full URL's for GET- and POST-trees<br>
  - getURL<br>
    - Like the name says it's the URL for the left GET-tree (loaded from yml - modified by app on demand)<br>
  - postURL
    - Like the name says it's the URL for the right POST-tree (loaded from yml - modified by app on demand)<br>
  - auth<br>
  You can do your authentication by username and password and getting a token. Many implementations exist so the tool cannot handle every kind of it.
  For the "normal" handling of token and refresh token things (in many examples) the app is armed. In most cases you don't really need the refresh token because if you leave the options blank a normal login is done every five seconds.<br>
  Most properties can be edited (tested) in login Dialog by clicking on the login button.<br>
    - loginURL<br>
      - The URL u call to login by username and password JSON.<br>
    - userProp<br>
      - The users property in your JSON file (e.g. username, email, ...)<br>
    - pwProp<br>
       - The password property in your JSON file (e.g. pw, password, secret, ...)<br>
    - userName<br>
       - Your real username.<br>
    - pw <br>
       - Your real password.<br>
    - headerTokenProp<br>
       - The authorization tokens Property. (normally Authorization)<br>
    - headerTokenPrefix<br>
       - Tokens text prefix. (often Bearer, secret, Token...)<br>
    - tokenProp<br>
       - The token property in answered json. (normally token)<br>
    - refreshTokenURL<br>
       - URL to refreshToken (same header as normal token).<br>
    - refreshTokenProp<br>
       - The refresh token property in answered json. (mostly refreshToken)<br>
    - additionalHeaders (not in GUI)<br>
       - You can add every additional header you need here. It will be sent with every request.
         The json headers Content-Type: application/json and accept: application/json are added automatically. It's jsonator! :)<br>

# further ideas
- There are many further things I could imagine to do with the tool. I will mention a few.<br>
- Batch mode<br>
  - You could define a format to automate processes with the tool.<br>
- There are more HTTP Methods to implement.<br>
- Save/Configure a list of endpoints and show a combobox.<br>

# Disclaimer
Like mentioned before. I absolutely don't guarantee anything. Use the tool at your own risk.
This is a fun project and my first JavaFX tool. So please don't beat me for mistakes. At last, sorry for my bad english I'm not a native speaker.<br>
  
